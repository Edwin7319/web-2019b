import {Module} from '@nestjs/common';
import {AutosController} from './autos.controller';
import {AutosService} from './autos.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {AutosEntity} from './autos.entity';

@Module({
    imports: [
        // modules
        // TypeOrmModule.forFeature([AutosEntity],'default')
    ],
    controllers: [
        AutosController
        // controladores
    ],
    providers: [
        AutosService
        // validarCedulaService
        // servicios
    ],
})
export class AutosModule {
}
