import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { FabricantesEntity } from "../fabricantes/fabricantes.entity";

@Entity('auto')//nombre de la tabla
export class AutosEntity{
    @PrimaryGeneratedColumn()
    id?:number;

    @Column({
        type:"varchar",
        length:20,
        name:'nombre_auto'
    })
    nombre:string;
    
    @Column({
        type:"varchar",
        length:10,
        name:'tipo'
    })
    tipo:'Deportivo'|'Camioneta'|'Taxi'|'Camion'|'Bus';;
    
    @Column({
        type:'int',
        name:'anio'
    })
    anio:number;
    
    @Column({
        type:'date',
        name:'fecha',
        default:'2020-01-28'
    })
    fecha: Date;
    
    @Column({
        type:'decimal',
        precision:10,
        scale:2,
        name:'precio',
        nullable:true
    })
    precio:number;

    @ManyToOne(type=>FabricantesEntity, fabricante=>fabricante.autos)
    fabricante:FabricantesEntity;
}