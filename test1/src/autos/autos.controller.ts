import {Controller, Get, Res, Post, Body, Session, Param, Req} from '@nestjs/common';
import {AutosService} from './autos.service';
import {Auto} from './interfaces/auto';

@Controller()
export class AutosController {
    constructor(private readonly _autosService: AutosService) {
    }

    @Get('lista')
    listarAutos(
        @Res() res,
        @Session() session
    ) {
        const arreglo = this._autosService.bddAutos;
        if (session.username != null) {
            res.render('autos/lista-autos', {arregloAutos: arreglo, user: session.username});
        } else {
            res.render('login');
        }
    }

    @Get('crear')
    crearAutos(
        @Res() res) {
        res.render('autos/crear-auto');
    }

    @Post('crear')
    crearAutoPost(
        @Body() auto: Auto,
        @Res()res) {
        auto.anio = Number(auto.anio);
        auto.precio = Number(auto.precio);
        const fecha = new Date(auto.fecha);
        this._autosService.crear(auto);
        console.log(formatDate(auto.fecha) + fecha);
        res.redirect('/lista');

        function formatDate(date) {
            let d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            if (month.length < 2) {
                month = '0' + month;
            }
            if (day.length < 2) {
                day = '0' + day;
            }

            return [year, month, day].join('-');
        }
    }

    @Get('editar/:idAuto')
    actualizarAutoVista(
        @Res() response,
        @Param('idAuto') idAuto: string,
    ) {
        const autoEncontrado = this._autosService.buscarPorId(+idAuto);
        response.render(
            'autos/editar-auto',
            {
                auto: autoEncontrado
            }
        );
    }

    @Post('editar/:idAuto')
    actualizarAuto(
        @Res() res,
        @Param('idAuto') idAuto: String,
        @Body() auto: Auto
    ) {
        auto.id = +idAuto;
        this._autosService.actualizar(auto, +idAuto);
        res.redirect('/lista');

    }

    @Post('buscar')
    buscar(
        @Res() res,
        @Req() req,
        @Body('busqueda') busqueda: string
    ) {
        const listaBusqueda: Auto[] = this._autosService.buscarPorNombre(busqueda);
        if (listaBusqueda != null) {
            res.render('autos/lista-autos', {arregloAutos: listaBusqueda});
        } else {
            res.render('/lista');
        }
    }

    @Post('eliminar')
    eliminarAuto(
        @Res() res,
        @Body('id') id: string) {
        this._autosService.eliminarPorId(Number(id));
        res.redirect('/lista');
    }
}
