export interface Auto{
    id?: number;
    nombre: string;
    tipo: 'Deportivo'|'Camioneta'|'Taxi'|'Camion'|'Bus';
    anio: number;
    fecha: Date;
    precio: number;
}