import {Controller, Get, Headers, Response, Session, Req, Body, Query, Delete, Res, Post} from '@nestjs/common';
import {AppService} from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    /*
      @Get()
      getHello(): string {
        return this.appService.getHello();
      }
    */
    @Get('/')
    index(
        @Res() res) {
        res.render('login');
    }

    @Get('login')
    loginVista(
        @Res() res
    ) {
        res.render('login');
    }

    @Get('session')
    session(
        @Query('nombre') nombre,
        @Session() session
    ) {
        console.log('En session ' + session);
        session.autenticado = true;
        session.nombreUsuario = nombre;
        return 'ok';

    }

    @Post('login')
    login(
        @Body() usuario,
        @Res() res,
        @Session() session
    ) {
        if (usuario.username === 'edwin' && usuario.password === '1234') {
            console.log(usuario);
            session.username = usuario.username;
            console.log(session);
            res.redirect('/lista');
        } else {
            res.status(400);
            res.send({mensaje: 'Error login', error: 400});
        }

    }

    /*
      @Get('lista')
      lista(
        @Session() session,
        @Res() res
        ){
          if(session.username){
            res.render('autos/lista-autos',{nombre:session.username});
          }else{
            res.redirect('/login');
          }

      }
    */
    @Get('logout')
    logout(
        @Res() res,
        @Session() session
    ) {
        session.username = undefined;
        session.destroy();
        res.redirect('/login');
    }

    @Get('suma')
    suma(@Headers() headers, @Response() res) {
        console.log(headers);
        if (headers.numerouno != null && headers.numerodos != null) {
            const totalsuma = Number(headers.numerouno) + Number(headers.numerodos);
            return res.status(500).send('Suma es: ' + totalsuma);
        } else {
            return res.status(400).send({mensaje: 'Error', error: 400});
        }
    }

    @Delete('division')
    division(
        @Headers() headers,
        @Body() body,
        @Query() query,
        @Response() res) {
        console.log(headers);
        console.log(query);
        if (headers.numerouno != null && body.numerodos == null && query.numerodos != null) {
            const totaldiv = Number(headers.numerouno) / Number(query.numerodos);
            return res.status(200).send('Division es: ' + totaldiv);
        } else {
            return res.status(400).send({mensaje: 'Error', error: 400});
        }
    }

    @Get('cookie')
    obtenerCookie(
        @Req() req,
        @Response() res) {
        const cook = req.cookies;
        console.log(cook);
        res.cookie(
            'fecha',
            new Date().toString()
        );
        res.cookie(
            'name',
            'Paul'
        );

        res.cookie(
            'segura',
            2,
            {signed: true}
        );
        const cookieSegura = req.signedCookies.segura;
        console.log(req.signedCookies);
        if (cookieSegura) {
            console.log('Cookie segura', cookieSegura);
        } else {
            console.log('Cookie no valida');
        }
        res.send(cook);
    }
}
