import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {NestExpressApplication} from '@nestjs/platform-express';
import {join} from 'path';

// tslint:disable-next-line:no-var-requires
const cookieParser = require('cookie-parser');
import * as session from 'express-session';

// tslint:disable-next-line:no-var-requires
const FileStore = require('session-file-store')(session);

async function bootstrap() {
    const app = await NestFactory.create(AppModule) as NestExpressApplication;
    app.use(
        session({
            name: 'server-session-id',
            secret: 'my secret',
            resave: false,
            saveUniinitialized: true,
            cookie: {
                secure: false
            },
            store: new FileStore()
        })
    );
    app.use(cookieParser('secret'));
    app.setViewEngine('ejs');
    app.setBaseViewsDir(join(__dirname, '..', 'views'));
    await app.listen(3000);
}

bootstrap();
