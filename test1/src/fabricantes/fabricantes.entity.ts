import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { AutosEntity } from "src/autos/autos.entity";

@Entity('fabricante')//nombre de la tabla
export class FabricantesEntity{
    @PrimaryGeneratedColumn()
    id:number;

    @Column({
        type:"varchar",
        length:30,
        name:'nombre'
    })
    nombre:string;
    
    @OneToMany(type => AutosEntity, auto=>auto.fabricante)
    autos:AutosEntity;
}