import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {AutosModule} from './autos/autos.module';
import {TypeOrmModule} from '@nestjs/typeorm';
import {AutosEntity} from './autos/autos.entity';
import {FabricantesEntity} from './fabricantes/fabricantes.entity';

@Module({
    imports: [AutosModule,
        // TypeOrmModule.forRoot({
        //     name: 'default',//nombre de la conexion, puedes tener varias bases la primera siempre default
        //     type: 'mysql',
        //     host: 'localhost',
        //     port: 3306,
        //     username: 'hernan',
        //     password: 'david',
        //     database: 'autos',
        //     entities: [AutosEntity, FabricantesEntity],//todas las entidades de la bdd
        //     synchronize: true,//cambios en las entidades se reflejen en la BDD
        //     dropSchema: true//borrar la bdd y crear por cambios en las entities
        // }),
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
